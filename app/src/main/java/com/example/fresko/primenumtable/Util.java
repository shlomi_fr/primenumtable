package com.example.fresko.primenumtable;

/**
 * Created by fresko on 01/04/2018.
 */

public class Util {

    public static boolean isPrime(int n) {
        if (n == 2) return true;
        //check if n is a multiple of 2
        if (n % 2 == 0 || n == 1)
            return false;

        //if not, then just check the odds
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    public static boolean hasCommonDivisor(int number1, int number2) {
        return findGCD(number1, number2) != 1;
    }

    private static int findGCD(int number1, int number2) {
        //base case
        if (number2 == 0) {
            return number1;
        }
        return findGCD(number2, number1 % number2);
    }


}
