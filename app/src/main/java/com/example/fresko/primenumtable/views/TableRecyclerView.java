package com.example.fresko.primenumtable.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.example.fresko.primenumtable.adapter.ItemsAdapter;

/**
 * Created by SHLOMI FRESKO on 10/04/2018.
 */

public class TableRecyclerView extends RecyclerView {
    public TableRecyclerView(Context context) {
        super(context);
    }

    public TableRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TableRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {

        final int action = MotionEventCompat.getActionMasked(e);

        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            // Release the scroll.
            return false; // Do not intercept touch event, let the child handle it
        }
        return super.onInterceptTouchEvent(e);
    }


}
