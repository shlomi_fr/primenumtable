package com.example.fresko.primenumtable.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fresko.primenumtable.R;
import com.example.fresko.primenumtable.Util;

/**
 * Created by fresko on 01/04/2018.
 */

public class ItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int numLongClicked = -1;

    public ItemsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_layout, parent, false);
        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((ViewHolder) holder).numTextView.setText(position + "");

        int colorRes;

        if (numLongClicked == -1) {
            colorRes = Util.isPrime(position) ? R.color.red : R.color.white;
        } else {
            //Tile long clicked...
            colorRes = Util.hasCommonDivisor(position, numLongClicked) ? R.color.green :
                    Util.isPrime(position) ? R.color.red : R.color.white;
        }
        ((ViewHolder) holder).numTextView.
                setBackgroundColor(ContextCompat.getColor(context, colorRes));

        //Long click listener
        ((ViewHolder) holder).itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (position != RecyclerView.NO_POSITION) {
                    numLongClicked = position;

                    notifyItemRangeChanged(0, position - 1);
                    notifyItemRangeChanged(position + 1, position + 50);

                    return true;
                }
                return false;
            }
        });
        //Check when long click released
        ((ViewHolder) holder).itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                view.onTouchEvent(motionEvent);

                int event = motionEvent.getAction();
                /*
                switch (event) {
                    case MotionEvent.ACTION_MOVE:
                        Log.e("touch", "move");
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        Log.e("touch", "cancel");
                        break;
                    case MotionEvent.ACTION_DOWN:
                        Log.e("touch", "down");
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        Log.e("touch", "pointer_up");
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.e("touch", "up");
                        break;
                    default:
                        Log.e("touch", "other");
                        break;

                }
*/
                // We're only interested in when the button is released.
                if (event == MotionEvent.ACTION_UP || event == MotionEvent.ACTION_CANCEL) {
                    Log.w("touch", "ACTION_UP");
                    // We're only interested in anything if our speak button is currently pressed.
                    if (numLongClicked != -1) {
                        // Do something when the button is released.
                        numLongClicked = -1;
                        notifyDataSetChanged();
                    }
                }
                return false;
            }
        });
    }


    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView numTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            numTextView = (TextView) itemView.findViewById(R.id.num_text);


        }
    }
}
