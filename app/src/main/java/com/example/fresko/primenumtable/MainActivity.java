package com.example.fresko.primenumtable;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.example.fresko.primenumtable.adapter.ItemsAdapter;
import com.example.fresko.primenumtable.views.TableRecyclerView;

public class MainActivity extends AppCompatActivity {

    private Context myContext;
    private ItemsAdapter itemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myContext = this;
        initRecyclerView();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    private void initRecyclerView() {
        final RecyclerView recyclerView = getRecyclerView();

        itemsAdapter = new ItemsAdapter(myContext);

        recyclerView.setAdapter(itemsAdapter);
        int numberOfColumns = 5;
        recyclerView.setLayoutManager(new GridLayoutManager(myContext, numberOfColumns));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ///// UI Getters /////////////

    private RecyclerView getRecyclerView() {
        return (TableRecyclerView) findViewById(R.id.recycler);
    }
}
